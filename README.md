Snap Stack Simulation with AirSim
=================================

This package provides supporting scripts, documentation, and code for simulating the ACL Snap Stack in a high-fidelity simulation environment called AirSim.

[AirSim](https://microsoft.github.io/AirSim/) is an open-source simulation environment by Shital Shah at Microsoft Research. Using a custom phyiscs engine provided by AirLib, quadrotor dynamics can be simulated at a higher rate and in a separate thread than the rendering thread. Rendering is provided by Unreal Engine 4, an open-source game engine (a Unity environment has also been created). AirLib is packaged as a UE4 plugin and the entire AirSim simulation is managed from the GameMode control of UE4.

## Look and Feel

<div align="center">
  <img src=".gitlab/turn.gif" />
</div>

The above environment with a shed-like structure is from the `example` project, where a 3D model of an industrial building was downloaded and imported. The vehicle was controlled using the [`acl_joy`](https://gitlab.com/mit-acl/fsw/demos/acl_joy) package. The ROS graph of this scenario is shown below.

<div align="center">
  <img src=".gitlab/rqt_graph.png" width="100%" />
</div>

## Getting Started

The following instructions outline how to start an AirSim environment and connect to it with the snap-stack ROS autopilot.

### Choosing an Environment

The AirSim [GitHub repository](https://github.com/microsoft/AirSim) contains pre-built environments as part of its [releases](https://github.com/microsoft/AirSim/releases). These pre-built environments preclude the need for installing UE4 *(is this true?)*. However, the official AirSim repo does not have the code for snap-stack integration. This code can be found at [github.com/mit-acl/AirSim](https://github.com/mit-acl/AirSim). Therefore, to use the snap-stack, you cannot use the pre-built environments on the official Microsoft AirSim release page. Instead, **you can use the pre-built `example` environment located [here](https://www.dropbox.com/s/ssgmgeu2npmqxzt/example.zip?dl=0)**.

Alternatively, you can build everything from source by following the [official AirSim instructions for Linux](https://github.com/microsoft/AirSim/blob/master/docs/build_linux.md), but using the ACL fork of AirSim in its place. This would allow you to build the Blocks environment with snap-stack integration.

If you would like to build a new environment or customize an existing environment, see the `example` environment included in this repo (in [`ue4`](ue4)) and its instructions.

### Dependencies and Directories

1. Clone the ACL AirSim fork from [github.com/mit-acl/AirSim](https://github.com/mit-acl/AirSim) (not in a catkin workspace).
    - Follow `setup.sh` and `build.sh` [instructions](https://github.com/microsoft/AirSim/blob/master/docs/build_linux.md#build-unreal-engine-and-airsim).
    - *TODO: Need to check if UE4 is required for these steps?*
2. Following the standard [**Getting Started**](https://gitlab.com/mit-acl/fsw/snap-stack/snap_sim#getting-started) steps for the snap-stack.
3. In addition to the standard snap-stack repos, you will need to symlink to the ACL AirSim `ros` directory. This can be done with

    ```bash
    $ cd your_ws/src
    $ ln -s /path/to/AirSim/ros/src airsim
    ```

4. When you are done, your directory structure may look like

    ```bash
    UnrealEngine # not required if using pre-built binaries (is true?)
    AirSim # make sure this is the github.com/mit-acl/AirSim fork
    simulation_ws
    └── src
        ├── acl_joy
        ├── airsim -> ../../AirSim/ros/src
        ├── outer_loop
        ├── snap
        ├── snap_airsim
        ├── snap_sim
        └── snapstack_msgs
    ```

5. Make sure to `catkin build`.

### Running the Simulation

Once you have access to an environment, either pre-built or through the UE4 editor, follow the steps below to get the AirSim SIL running.

1. Setup the vehicle configuration by copying or linking the [`settings.json`](config).
2. Start the game, e.g., for the pre-built `example` environment run: `./example.sh -ResX=640 -ResY=480 -windowed`.
    - This will start the game in a small window. You can make it full screen with ALT+ENTER.
    - Note that the window will be black and unresponsive. This is because it is attempting to connect to the `snap` autopilot.
3. Start the `snap` autopilot and simulation support node: `roslaunch snap_airsim sim.launch`.
4. Start the snap-stack supporting code (see [`snap_sim`](https://gitlab.com/mit-acl/fsw/snap-stack/snap_sim) and [`acl_joy`](https://gitlab.com/mit-acl/fsw/demos/acl_joy) for more). In three separate terminal windows:

    ```bash
    $ roslaunch snap esc.launch veh:=AS num:=01s
    $ roslaunch outer_loop cntrl.launch veh:=AS num:=01s
    $ roslaunch acl_joy key.launch veh:=AS num:=01s
    ```

5. We still need to run the AirSim ROS bridge. Make sure that the snap-stack autopilot is fully connected before running this command (i.e., the black window is now showing a multirotor sitting on the ground).

    ```bash
    $ roslaunch snap_airsim airsim_ros.launch
    ```

6. As usual (see [`snap_sim`](https://gitlab.com/mit-acl/fsw/snap-stack/snap_sim) and [`snap`](https://gitlab.com/mit-acl/fsw/snap-stack/snap) if necessary), arm, takeoff, and fly!

**Hint**: Take a look at [this image](.gitlab/terminator_layout.png), which suggests a useful terminal layout and details the order of execution.

## FAQ

1. I am getting strange errors when trying to start the AirSim simulator or the `snap_airsim sim.launch` file.

    - This is likely due to shared memory problems. Take a look at `ipcs`. Anything that does not have `dest` in the last column and is around 100-200 bytes of memory is likely left over shared memory from a failed (or improperly terminated) process. Use `ipcrm -m <shmid>` to destroy that block of shared memory.

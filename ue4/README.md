Unreal Environments
===================

*Tested on Ubuntu 18.04 and UE4.22*

Here are custom Unreal environments that are "AirSim enabled". Being AirSim enabled amounts to including the AirLib UE4 plugin. Therefore, any Unreal project can be used for AirSim simulation. The UE4 projects in this repo are tabulated below. Additionally, this README gives resources and an overview of how to create an environment with the AirSim plugin.

| Project Name | Comments                                                               |
|--------------|------------------------------------------------------------------------|
| example      | A blank project with a model of a run-down shed imported from cgtrader |

## Guides - Example Project

### Creating a Custom Environment and Importing Assets 

1. Start UE4Editor.
2. Create a Blank Project. Select 'No Starter Content'.
3. Download [this](https://www.cgtrader.com/free-3d-models/exterior/office/indusrtial-building) low-poly 3D model of an "indusrtial building" (sic) in `*.fbx` format (account required). Rename the `untitled.fbx` to `building.fbx`.
4. Watch these YouTube videos (and probably others) about importing assets:
    - [How to Import a Static Mesh (3D Model) into Unreal Engine 4](https://www.youtube.com/watch?v=fLo6zFllSRw)
    - [Create the Materials for the Cabin](https://www.youtube.com/watch?v=RdlQVSzA1fg)
4. We will need to import this into our project. There are two primary ways to do this.
    1. Use the **Import** button at the bottom in the **Content Browser** (or just drag-and-drop---doesn't work on Ubuntu).
    2. Import the `*.fbx` [as a scene](https://docs.unrealengine.com/en-US/Engine/Content/Importing/FBX/FullScene/index.html). Not sure when you should or shouldn't do this. There seems to be something about the way the `*.fbx` was created? This lets you import the entire scene into a blueprint rather than importing each asset individually and creating the final scene yourself. **File > Import into Level**.
5. Use option **2. Import into Level** to import `building.fbx`. Create a new folder `Content/Building` to import the assets into. Accept defaults. Ignore warnings. Watch [this video]() to see what I did.

#### Miscellaneous

See resources / our walkthrough videos for these.

- Make sure collisions are good
- Turn off Eye Adaptation (auto exposure)
- Make sure you can see Plugin contents in **Content Browser**: [AirSim docs](https://github.com/microsoft/AirSim/blob/master/docs/working_with_plugin_contents.md)
- Editor settings: [disable *Use Less CPU when in Background*](https://github.com/microsoft/AirSim/blob/master/docs/apis.md#unreal-is-slowed-down-dramatically-when-i-run-api)
- Sometimes you have to rebuild lighting (click **Build** at top tool strip)

### Adding the AirSim Plugin to Your UE4 Project

1. Close the UE4Editor that is running your project.
2. Use `cp_airlib_to_ue4_project.sh /path/to/AirSim ./example` to copy the AirSim UE4 Plugin into your project.
3. Open your project again: `./Editor/Binaries/Linux/UE4Editor /path/to/snap_airsim/ue4/example/example.uproject`
4. Click **Yes** when asked about building **AirSim**.
5. Make sure that `AirSimGameMode` is set in World Settings
6. You should be able to pree **Play** in the editor and follow the instructions as normal (see [main README](/)).

### Cooking - Packaging to Standalone Linux Execution

- Talk about necessary settings (use Pak, compress package, use specific maps--example and AirSimAssets)
- Making an `AirSimAssets.umap` with a **AirSim Content > Blueprints > BP_FlyingPawn** actor (in Plugin contents)
- windowed mode
- turning off auto capture mouse, lock mouse, etc. in **Input** settings.
- Cook: **File > Package Project > Linux**

### Additional Resources

In addition to the resources referred to above, check these out. Each resource is prepended with value/importance, 1 being low, 5 being high.

- (5) YouTube: [Unreal AirSim Setup from scratch](https://www.youtube.com/watch?v=1oY8Qu5maQQ)
- (2) AirSim docs: [Creating and Setting Up Unreal Environment](https://github.com/microsoft/AirSim/blob/master/docs/unreal_custenv.md) (follows above YouTube video)
- (2) YouTube: [Importing 3rd Part Environments in Unreal Engine for AirSim](https://www.youtube.com/watch?v=oR-LXzWENYg)
- (1) UE4 Docs: [Import Into Level (FBX Scene Import)](https://docs.unrealengine.com/en-US/Engine/Content/Importing/FBX/FullScene/index.html)
- (1) GitHub: [UE4LinuxLauncher](https://github.com/Erlandys/UE4LinuxLauncher)
- (5) YouTube: [How to Disable Auto Exposure (Eye Adaptation) in UE4](https://www.youtube.com/watch?v=K2kIre_BHgA)
- (3) YouTube: [UE4 Tutorial - Collision (Quick & Easy)](https://www.youtube.com/watch?v=XLvJnFUk3Cc)
- (4) YouTube: [UE4 Simple vs Complex Collision](https://www.youtube.com/watch?v=blnND6zs2RU)

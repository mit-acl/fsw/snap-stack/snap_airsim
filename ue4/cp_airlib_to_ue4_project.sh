#!/bin/bash

AIRSIM_DIR=$1
UE4PROJECT=$2
AIRSIMPLUGIN=$AIRSIM_DIR/Unreal/Plugins/AirSim

/bin/bash $UE4PROJECT/clean.sh
mkdir -p $UE4PROJECT/Plugins
rsync -a --delete $AIRSIMPLUGIN $UE4PROJECT/Plugins

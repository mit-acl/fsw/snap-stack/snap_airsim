AirSim Configuration
====================

AirSim uses the `settings.json` file to configure the simulation environment on initialization. To reload the settings after an edit, restart the simulator.

These settings provide basic functionality for using the snap stack autopilot to fly a quadrotor equipped with an RGBD sensor. It creates a simulated vehicle with the name `AS01s` (see [snap_sim](https://gitlab.com/mit-acl/fsw/snap-stack/snap_sim) for more info on naming)). Copying this file to the AirSim settings location (`~/Documents/AirSim/settings.json` on Linux) is encouraged. If you plan to make generally useful changes, consider making a symlink with

```bash
# Do this if you want to source control your settings changes 
~/Documents/AirSim $ ln -s ~/path/to/snap_airsim/config/settings.json
```

## FAQ

1. How do I know what settings are available to be set?

    See [here](https://github.com/microsoft/AirSim/blob/master/docs/settings.md).

2. How is the vehicle name determined?

    The key of each vehicle dict within the `Vehicles` dictionary ends up being used as the vehicle name.

3. How do I know which autopilot is being used?

    Paired with a [`SimMode`](https://github.com/microsoft/AirSim/blob/master/docs/settings.md#how-to-chose-between-car-and-multirotor): `Multirotor`, a [`VehicleType`](https://github.com/microsoft/AirSim/blob/master/docs/settings.md#common-vehicle-setting) of `SnapStack` will use the ACL snap stack autopilot.

4. How do I add cameras?
  
    Cameras are found in the dict keyed by `Cameras`. The camera names, which are the keys of the child dicts, must be an [available camera](https://github.com/microsoft/AirSim/blob/master/docs/image_apis.md#multirotor).

5. How do I manage camera poses?

    Although cameras have names like `front_center` and `front_left`, they seem to always be initialized at the body frame. Therefore, you can set each camera's pose w.r.t the body using the pose keys (`X`, `Pitch`, etc.)

6. Headless mode?

    *Marginal* efficiency gains can be had by using a headless mode, where we turn off rendering of the external HUD (cameras streams are still rendered). This can be done by changing [`ViewMode`](https://github.com/microsoft/AirSim/blob/master/docs/settings.md#viewmode) from the default `FlyWithMe` to `NoDisplay`

7. Coordinate frames, origins, and initial poses.

    For general coordinate frame information, see the [main README](../). The AirSim inertial/world coordinate frame is determined by the Player Start location in the Unreal environment (transformed from Unreal Coordinates to NED). Thus, by default, the multirotor in the **Blocks** environment starts and then falls about 0.6712 m to the ground. Also, since we convert NED/frd to ENU/flu, we set the intial orientation of the vehicle so that body-x is aligned with world-E (i.e., identity pose seen by autopilot).